//
//  AppDelegate.h
//  拖拽手势画图
//
//  Created by MB__Lee on 2018/5/8.
//  Copyright © 2018年 MB__Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

